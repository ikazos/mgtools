# `mgtools`

For parsing and writing out MGs (Minimalist Grammars) in a slightly opinionated format.  See `tests/simple.mg` for an example.

# How to use

Add this to your `Cargo.toml`:

```
[dependencies]
mgtools = { git = "https://gitlab.com/ikazos/mgtools.git" }
```

You can also use specify a tag.

```
[dependencies]
mgtools = { git = "https://gitlab.com/ikazos/mgtools.git", tag = "0.1.0" }
```

# Docs

They should be hosted [here](https://ikazos.gitlab.io/mgtools/mgtools).