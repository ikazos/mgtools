# Implementation details

This document describes how things are implemented in this crate.

# Parsing and writing representations

Each module (e.g. `feature`, `lexical_entry`, `mg`, ...) defines some type, which is a MG-related representation.  This type should implement three traits: `std::fmt::Display`, `Parse` and `std::str::FromStr`.

## `Parse`

Implementing the `Parse` trait on a type `T` requires you to define `parse`, a parsing function that takes a `logos::Lexer<Token>`, an iterator over tokens, and returns a `(T, usize)`.  The `usize` is the number of tokens you consumed.  You should return `ParseError` if parsing fails.  This `parse` function should be greedy, i.e., it should consume as many tokens as possible.  For example, say the input is:

```
"who" :: d -wh
```

In token terms, this is:

```
DoubleQuoteLiteral("who"), DoubleColon, Id("d"), MinusId("wh")
```

And you want to parse this into a lexical entry.  There are two possibilities: one that includes "-wh" as part of the entry and one that doesn't.  A good implementation of `Parse` should include that feature.  The syntax for the representations covered by this crate is designed so that this kind of greedy parsing doesn't cause a problem.

For simple representations, you could just write the parser from scratch.  But for more complex representations that consist of other types that implement `Parse`, maybe you want to use their `Parse::parse` to implement `Parse` for the more complex type.  See `feature::Feature` for an example of the former case, and `lexical_entry::LexicalEntry` for an example of the latter case.

## `std::from::FromStr`

Given a type `T` implements `Parse`, there is a simple way to implement `FromStr` for `T`: convert the input string into tokens and parsing them using `Parse::parse`.  This is what the `impl_from_str!` macro does, and you can simply use it to get a `FromStr` implementation for free.

## `std::fmt::Display`

The `Display` implementation should write out a type in a way that can be `Parse::parse`d back into itself.
