use nom::{multi::many1, IResult};
use nom_locate::LocatedSpan;

use crate::{
    expected_double_colon, expected_literal, feature::Feature, impl_from_str, multispace1_then, Parse, ParseError
};

use std::fmt;



/// A lexical entry.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct LexicalEntry {
    pf: String,
    fseq: Vec<Feature>,
}



impl LexicalEntry {
    /// Create a new lexical entry from its PF and feature sequence.
    pub fn new(pf: String, fseq: Vec<Feature>) -> Self {
        Self { pf, fseq }
    }

    /// Get a reference to the PF of this lexical entry.
    pub fn pf(&self) -> &String {
        &self.pf
    }

    /// Get a mutable reference to the PF of this lexical entry.
    pub fn pf_mut(&mut self) -> &mut String {
        &mut self.pf
    }

    /// Get a reference to the feature sequence of this lexical entry.
    pub fn fseq(&self) -> &Vec<Feature> {
        &self.fseq
    }

    /// Get a mutable reference to the feature sequence of this lexical entry.
    pub fn fseq_mut(&mut self) -> &mut Vec<Feature> {
        &mut self.fseq
    }
}



impl fmt::Display for LexicalEntry {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if !self.pf.is_empty() {
            write!(f, "\"{}\" ", self.pf)?;
        }

        write!(f, ":: ")?;

        for (k, x) in self.fseq.iter().enumerate() {
            if k == 0 {
                write!(f, "{}", x)?;
            }
            else {
                write!(f, " {}", x)?;
            }
        }

        Ok(())
    }
}



impl Parse for LexicalEntry {
    fn subparser_name() -> String {
        format!("LexicalEntry")
    }

    fn parse(
        input: LocatedSpan<&str>
    ) -> IResult<LocatedSpan<&str>, Self, ParseError> {
        let (input, pf) = expected_literal(input)?;

        let (input, _) = multispace1_then(expected_double_colon)(input)?;

        let (input, fseq) = many1(
            multispace1_then(<Feature as Parse>::subparse)
        )(input)?;

        Ok((input, LexicalEntry { pf, fseq }))
    }
}

impl_from_str!(LexicalEntry);



#[cfg(test)]
mod tests {
    use std::str::FromStr;

    use crate::{feature::Feature, lexical_entry::LexicalEntry};


    #[test]
    fn simple() {
        assert_eq!(
            LexicalEntry::from_str(r#""john" :: d -case"#),
            Ok(LexicalEntry {
                pf: format!("john"),
                fseq: vec![
                    Feature::Selectee(format!("d")),
                    Feature::Licensee(format!("case")),
                ],
            })
        )
    }

}
