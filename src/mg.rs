use nom::{multi::many0, sequence::terminated, IResult};
use nom_locate::LocatedSpan;

use crate::{
    expected_id, expected_semicolon, feature::Feature, impl_from_str, lexical_entry::LexicalEntry, multispace0_then, Parse, ParseError
};

use std::{
    collections::HashSet, fmt
};



/// A MG (Minimalist Grammar).
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct MG {
    starting_category: String,
    syntactic_types: Vec<String>,
    licensing_types: Vec<String>,
    lexicon: Vec<LexicalEntry>,
}



impl MG {
    /// Create a new MG from its starting category, a vector of syntactic types, a vector of licensing types and a lexicon (represented by a vector of lexical entries).
    pub fn new(
        starting_category: String,
        syntactic_types: Vec<String>,
        licensing_types: Vec<String>,
        lexicon: Vec<LexicalEntry>
    ) -> Self {
        Self {
            starting_category,
            syntactic_types,
            licensing_types,
            lexicon,
        }
    }

    /// Get a reference to the starting category of this MG.
    pub fn starting_category(&self) -> &String {
        &self.starting_category
    }

    /// Get a mutable reference to the starting category of this MG.
    pub fn starting_category_mut(&mut self) -> &mut String {
        &mut self.starting_category
    }

    /// Get a reference to the vector of syntactic types of this MG.
    pub fn syntactic_types(&self) -> &Vec<String> {
        &self.syntactic_types
    }

    /// Get a mutable reference to the vector of syntactic types of this MG.
    pub fn syntactic_types_mut(&mut self) -> &mut Vec<String> {
        &mut self.syntactic_types
    }

    /// Get a reference to the vector of licensing types of this MG.
    pub fn licensing_types(&self) -> &Vec<String> {
        &self.licensing_types
    }

    /// Get a mutable reference to the vector of licensing types of this MG.
    pub fn licensing_types_mut(&mut self) -> &mut Vec<String> {
        &mut self.licensing_types
    }

    /// Get a reference to the lexicon of this MG.
    pub fn lexicon(&self) -> &Vec<LexicalEntry> {
        &self.lexicon
    }

    /// Get a mutable reference to the lexicon of this MG.
    pub fn lexicon_mut(&mut self) -> &mut Vec<LexicalEntry> {
        &mut self.lexicon
    }
}



impl fmt::Display for MG {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "{};", self.starting_category)?;

        for le in self.lexicon.iter() {
            writeln!(f, "{};", le)?;
        }

        Ok(())
    }
}



impl Parse for MG {
    fn subparser_name() -> String {
        format!("MG")
    }

    fn parse(
        input: LocatedSpan<&str>
    ) -> IResult<LocatedSpan<&str>, Self, ParseError> {
        let (input, starting_category) = terminated(
            expected_id,
            multispace0_then(expected_semicolon)
        )(input)?;

        let mut selecting_types = HashSet::new();
        let mut licensing_types = HashSet::new();

        let (input, lexicon) = many0(
            terminated(
                multispace0_then(<LexicalEntry as Parse>::subparse),
                multispace0_then(expected_semicolon)
            )
        )(input)?;

        for lexical_entry in &lexicon {
            for f in lexical_entry.fseq() {
                match f {
                    Feature::Selector(subf) |
                    Feature::Selectee(subf) =>
                        selecting_types.insert(subf.clone()),
                        
                    Feature::Licensor(subf) |
                    Feature::Licensee(subf) =>
                        licensing_types.insert(subf.clone()),
                };
            }
        }

        let mg = MG::new(
            starting_category,
            selecting_types.into_iter().collect(),
            licensing_types.into_iter().collect(),
            lexicon
        );

        Ok((input, mg))
    }
}

impl_from_str!(MG);



#[cfg(test)]
mod tests{
    use std::str::FromStr;

    use crate::mg::MG;


    #[test]
    fn simple() {
        let mg = MG::from_str(include_str!("../tests/simple.mg"))
            .unwrap_or_else(|e| {
                panic!("{}", e);
            });

        assert_eq!(
            mg.starting_category,
            format!("c")
        );



    }

}