use nom::{bytes::complete::tag, combinator::map, sequence::preceded, IResult};
use nom_locate::LocatedSpan;

use crate::{
    expected_alt, expected_id, Parse, ParseError
};

use std::fmt;



/// A feature.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Feature {
    /// A selector feature `=x`, given a string `x` that denotes a selecting type.
    Selector(String),
    /// A selectee feature `x`, given a string `x` that denotes a selecting type.
    Selectee(String),
    /// A licensor feature `+x`, given a string `x` that denotes a licensing type.
    Licensor(String),
    /// A licensee feature `-x`, given a string `x` that denotes a licensing type.
    Licensee(String),
}



impl fmt::Display for Feature {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Feature::Selector(x) => write!(f, "={}", x),
            Feature::Selectee(x) => write!(f, "{}", x),
            Feature::Licensor(x) => write!(f, "+{}", x),
            Feature::Licensee(x) => write!(f, "-{}", x),
        }
    }
}



fn parse_selector(
    input: LocatedSpan<&str>
) -> IResult<LocatedSpan<&str>, Feature, ParseError> {
    preceded(
        tag("="),
        map(
            expected_id,
            |f| Feature::Selector(f)
        )
    )(input)
}



fn parse_licensor(
    input: LocatedSpan<&str>
) -> IResult<LocatedSpan<&str>, Feature, ParseError> {
    preceded(
        tag("+"),
        map(
            expected_id,
            |f| Feature::Licensor(f)
        )
    )(input)
}



fn parse_licensee(
    input: LocatedSpan<&str>
) -> IResult<LocatedSpan<&str>, Feature, ParseError> {
    preceded(
        tag("-"),
        map(
            expected_id,
            |f| Feature::Licensee(f)
        )
    )(input)
}



fn parse_selectee(
    input: LocatedSpan<&str>
) -> IResult<LocatedSpan<&str>, Feature, ParseError> {
    map(
        expected_id,
        |f| Feature::Selectee(f)
    )(input)
}



impl Parse for Feature {
    fn subparser_name() -> String {
        format!("Feature")
    }

    fn parse(
        input: LocatedSpan<&str>
    ) -> IResult<LocatedSpan<&str>, Self, ParseError> {
        expected_alt(
            format!("one of: equal sign \"=\", plus sign \"+\", minus sign \"-\" or identifier"),
            (
                parse_selector,
                parse_licensor,
                parse_licensee,
                parse_selectee,
            )
        )(input)
    }
}

impl_from_str!(Feature);



#[cfg(test)]
mod tests {

    use std::str::FromStr;

    use crate::feature::Feature;

    #[test]
    fn simple() {
        assert_eq!(
            Feature::from_str("=d"),
            Ok(Feature::Selector(format!("d")))
        );
        
        assert_eq!(
            Feature::from_str("asp"),
            Ok(Feature::Selectee(format!("asp")))
        );
        
        assert_eq!(
            Feature::from_str("+wh"),
            Ok(Feature::Licensor(format!("wh")))
        );
        
        assert_eq!(
            Feature::from_str("-case"),
            Ok(Feature::Licensee(format!("case")))
        );

        eprintln!("{:?}", Feature::from_str("/case"));
    }

}
