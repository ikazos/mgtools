use std::fmt;

use nom::{combinator::map, multi::{many0, separated_list0}, sequence::terminated, IResult};
use nom_locate::LocatedSpan;

use crate::{expected_alt, expected_comma, expected_id, expected_left_paren, expected_right_paren, expected_semicolon, lexical_entry::LexicalEntry, multispace0_then, Parse, ParseError};





/// A derivation.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Derivation {
    Leaf(LexicalEntry),
    Branch {
        func: String,
        children: Vec<Derivation>,
    }
}



impl fmt::Display for Derivation {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Derivation::Leaf(lexical_entry) =>
                write!(f, "{}", lexical_entry),

            Derivation::Branch { func, children } => {
                write!(f, "{}(", func)?;
                for (k, child) in children.iter().enumerate() {
                    if k == 0 {
                        write!(f, "{}", child)?;
                    }
                    else {
                        write!(f, ",{}", child)?;
                    }
                }
                write!(f, ")")
            },
        }
    }
}



fn parse_leaf(
    input: LocatedSpan<&str>
) -> IResult<LocatedSpan<&str>, Derivation, ParseError> {
    map(
        <LexicalEntry as Parse>::subparse,
        |lexical_entry| Derivation::Leaf(lexical_entry)
    )(input)
}



fn parse_branch(
    input: LocatedSpan<&str>
) -> IResult<LocatedSpan<&str>, Derivation, ParseError> {
    let (input, func) = expected_id(input)?;
    let (input, _) = multispace0_then(expected_left_paren)(input)?;
    let (input, children) = separated_list0(
        multispace0_then(expected_comma),
        multispace0_then(<Derivation as Parse>::subparse)
    )(input)?;
    let (input, _) = multispace0_then(expected_right_paren)(input)?;

    Ok((input, Derivation::Branch { func, children }))
}



impl Parse for Derivation {
    fn subparser_name() -> String {
        format!("Derivation")
    }

    fn parse(
        input: LocatedSpan<&str>
    ) -> IResult<LocatedSpan<&str>, Self, ParseError> {
        expected_alt(
            format!("a lexical entry (leaf) or an identifier (branch)"),
            (parse_leaf, parse_branch)
        )(input)
    }
}

impl_from_str!(Derivation);



#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Derivations(pub Vec<Derivation>);



impl fmt::Display for Derivations {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for deriv in &self.0 {
            write!(f, "{};\n", deriv)?;
        }

        Ok(())
    }
}



impl Parse for Derivations {
    fn subparser_name() -> String {
        format!("Derivations")
    }

    fn parse(
        input: LocatedSpan<&str>
    ) -> IResult<LocatedSpan<&str>, Self, ParseError> {
        let (input, deriv) = terminated(
            <Derivation as Parse>::subparse,
            multispace0_then(expected_semicolon)
        )(input)?;

        let (input, mut derivs) = many0(
            terminated(
                multispace0_then(<Derivation as Parse>::subparse),
                multispace0_then(expected_semicolon)
            )
        )(input)?;

        derivs.insert(0, deriv);

        Ok((input, Derivations(derivs)))
    }
}

impl_from_str!(Derivations);



#[cfg(test)]
mod tests {

    use std::str::FromStr;

    use crate::deriv::Derivations;

    use super::Derivation;

    #[test]
    fn simple() {
        let s = r#"merge1("" :: =v c, merge1("left" :: =d v, "john" :: d))"#;

        eprintln!("{:?}", Derivation::from_str(s));
        assert!(false);
    }

    #[test]
    fn simpler() {
        eprintln!("{:?}", Derivations::from_str(include_str!("../tests/simple.derivs")));
        assert!(false);
    }

}
