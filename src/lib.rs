//! This crate has three main features:
//! 
//! *   **Parsing a [`mg::MG`] from a file**
//! 
//!     The function [`parse::parse`] reads in a `&str` and parses it into [`mg::MG`], a struct that represents a Minimalist Grammar.
//! 
//! *   **Manipulating MG-related representations**
//! 
//!     The [`mg`] module contains some types that represent MG and relevant constructs.
//! 
//! *   **Writing a [`mg::MG`] to a string**
//! 
//!     [`mg::MG`] implements [`std::fmt::Display`] in a way that matches how the [`parse::parse`] function expects it to be written in.  You can write a [`mg::MG`] to a string or a file with, e.g., `format!("{}", mg)` or `write!(f, "{}", mg)`.



use core::fmt;

use nom::{branch::{alt, Alt}, bytes::complete::tag, character::complete::{alphanumeric1, multispace0, multispace1}, combinator::{eof, map, recognize}, error::ErrorKind, multi::{many0, many1}, sequence::{delimited, preceded, terminated}, AsChar, Err, IResult, InputTakeAtPosition, Parser};
use nom_locate::{position, LocatedSpan};
use thiserror::Error;



/// Span information, used in `ParseError`.
/// 
/// This is equivalent to `nom_locate::LocatedSpan`, except it doesn't store a reference to the parsed string.  This means `Span` doesn't need to take a lifetime parameter, unlike `nom_locate::LocatedSpan`.  This is good because we can't let `Span` take a lifetime parameter because we use it in `ParseError`.  Otherwise, `ParseError` would also have to take a lifetime parameter, which would mean we can't use `ParseError` as the error type for the `FromStr` implementations we derive for types that implements `Parse`.  But we want to do that!
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct Span {
    offset: usize,
    line: usize,
    column: usize,
}



impl Span {
    fn new(offset: usize, line: usize, column: usize) -> Self {
        Self { offset, line, column }
    }

    fn offset(&self) -> usize {
        self.offset
    }

    fn line(&self) -> usize {
        self.line
    }

    fn column(&self) -> usize {
        self.column
    }
}



impl From<LocatedSpan<&str>> for Span {
    fn from(value: LocatedSpan<&str>) -> Self {
        Self::new(
            value.location_offset(),
            value.location_line() as usize,
            value.get_column()
        )
    }
}



impl fmt::Display for Span {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "ln {}, col {} (offset {})", self.line, self.column, self.offset)
    }
}



/// Parser error.
#[derive(Error, Debug, Clone, PartialEq, Eq)]
pub enum ParseError {
    #[error("expected {expected} at {span}")]
    Expected {
        expected: String,
        span: Span,
    },
    #[error("invoking subparser {subparser} at {span}, failed with the following error:\n{suberror}")]
    Subparser {
        subparser: String,
        span: Span,
        suberror: Box<ParseError>,
    },
    #[error("trailing garbage at {span}")]
    TrailingGarbage {
        span: Span,
    },
    #[error("unknown error, something is likely wrong with the `parse` function")]
    Unknown,
    #[error("the fact that you are even seeing this error variant is an error")]
    Unreachable,
}



impl<I> nom::error::ParseError<I> for ParseError {
    fn from_error_kind(_: I, _: ErrorKind) -> ParseError {
        ParseError::Unreachable
    }

    fn append(_: I, _: ErrorKind, s: Self) -> ParseError {
        s
    }
}



/// Create a `ParseError::Expected`-type error.  It doesn't propagate any errors.
/// 
/// Based on [`nom::error::context`](https://docs.rs/nom/latest/src/nom/error.rs.html#233-246).
fn context_expected<'a, F, O>(
    expected: String,
    mut f: F,
) -> impl FnMut(LocatedSpan<&'a str>) -> IResult<LocatedSpan<&'a str>, O, ParseError>
where F: Parser<LocatedSpan<&'a str>, O, ParseError>
{
    move |i| {
        let (i, span) = position(i.clone())?;
        
        match f.parse(i) {
            Ok(o) => Ok(o),
            Err(Err::Incomplete(i)) => Err(Err::Incomplete(i)),
            Err(Err::Error(_)) =>
                Err(Err::Error(ParseError::Expected {
                    expected: expected.clone(),
                    span: span.into(),
                })),
            Err(Err::Failure(_)) =>
                Err(Err::Failure(ParseError::Expected {
                    expected: expected.clone(),
                    span: span.into(),
                })),
        }
    }
}



/// Create a `ParseError::Subparser`-type error.
/// 
/// Based on [`nom::error::context`](https://docs.rs/nom/latest/src/nom/error.rs.html#233-246).
fn context_subparser<'a, O, F>(
    subparser: String,
    mut f: F,
) -> impl FnMut(LocatedSpan<&'a str>) -> IResult<LocatedSpan<&'a str>, O, ParseError>
where F: Parser<LocatedSpan<&'a str>, O, ParseError>
{
    move |i| {
        let (i, span) = position(i.clone())?;
        match f.parse(i) {
            Ok(o) => Ok(o),
            Err(Err::Incomplete(i)) => Err(Err::Incomplete(i)),
            Err(Err::Error(e)) =>
                Err(Err::Error(ParseError::Subparser {
                    subparser: subparser.clone(),
                    span: span.into(),
                    suberror: Box::new(e),
                })),
            Err(Err::Failure(e)) =>
                Err(Err::Failure(ParseError::Subparser {
                    subparser: subparser.clone(),
                    span: span.into(),
                    suberror: Box::new(e),
                })),
        }
    }
}



fn finish_with<'a, O, F>(
    mut parser: F
) -> impl FnMut(LocatedSpan<&'a str>) -> IResult<LocatedSpan<&'a str>, O, ParseError>
where F: Parser<LocatedSpan<&'a str>, O, ParseError>
{
    move |input| {
        let (input, res) = parser.parse(input)?;
        let (input, span) = position(input)?;

        match eof::<LocatedSpan<&str>, ParseError>(input) {
            Ok((input, _)) => Ok((input, res)),
            Err(_) => Err(Err::Error(ParseError::TrailingGarbage {
                span: span.into(),
            })),
        }
    }
}



fn multispace0_then<I: InputTakeAtPosition, O, E: nom::error::ParseError<I>, F>(
    parser: F
) -> impl FnMut(I) -> IResult<I, O, E>
where F: Parser<I, O, E>, <I as InputTakeAtPosition>::Item: AsChar + Clone
{
    preceded(multispace0, parser)
}



fn then_multispace0<I: InputTakeAtPosition, O, E: nom::error::ParseError<I>, F>(
    parser: F
) -> impl FnMut(I) -> IResult<I, O, E>
where F: Parser<I, O, E>, <I as InputTakeAtPosition>::Item: AsChar + Clone
{
    terminated(parser, multispace0)
}



fn multispace1_then<I: InputTakeAtPosition, O, E: nom::error::ParseError<I>, F>(
    parser: F
) -> impl FnMut(I) -> IResult<I, O, E>
where F: Parser<I, O, E>, <I as InputTakeAtPosition>::Item: AsChar + Clone
{
    preceded(multispace1, parser)
}



fn then_multispace1<I: InputTakeAtPosition, O, E: nom::error::ParseError<I>, F>(
    parser: F
) -> impl FnMut(I) -> IResult<I, O, E>
where F: Parser<I, O, E>, <I as InputTakeAtPosition>::Item: AsChar + Clone
{
    terminated(parser, multispace1)
}



/// Equivalent to `nom::branch::alt`, but fail with `ParseError::Expected`.
/// 
/// Since `ParseError` doesn't have a meaningful implementation of `nom::error::ParseError::append`, `nom::branch::alt` only fails with the errors thrown by the last alternative.  Using this function allows the developer to annotate what all the alternatives are with a message like "one of ...".
fn expected_alt<'a, O, List: Alt<LocatedSpan<&'a str>, O, ParseError>>(
    expected: String,
    l: List
) -> impl FnMut(LocatedSpan<&'a str>) -> IResult<LocatedSpan<&'a str>, O, ParseError>
{
    context_expected(
        expected,
        alt(l)
    )
}



fn expected_id(input: LocatedSpan<&str>) -> IResult<LocatedSpan<&str>, String, ParseError> {
    //  [_a-zA-Z0-9]+
    context_expected(
        format!("an identifier"),
        map(
            recognize(
                many1(
                    alt((
                        tag("_"),
                        alphanumeric1
                    ))
                )
            ),
            |s: LocatedSpan<&str>| s.to_string()
        )
    )(input)
}



fn expected_literal(input: LocatedSpan<&str>) -> IResult<LocatedSpan<&str>, String, ParseError> {
    //  [_a-zA-Z0-9]+
    context_expected(
        format!("an double quotes-enclosed literal"),
        map(
            delimited(
                tag("\""),
                recognize(
                    many0(
                        alt((
                            tag("_"),
                            alphanumeric1
                        ))
                    )
                ),
                tag("\""),
            ),
            |s: LocatedSpan<&str>| s.to_string()
        )
    )(input)
}



/*
fn semicolon(input: LocatedSpan<&str>) -> IResult<LocatedSpan<&str>, (), ParseError> {
    preceded(
        multispace0,
        context_expected(
            format!(";"),
            map(
                tag(";"),
                |_| ()
            )
        )
    )(input)
}
*/
macro_rules! make_expected_token_parser {
    ($parser_name:ident, $expected:literal, $tag:literal) => {
        fn $parser_name(input: LocatedSpan<&str>) -> IResult<LocatedSpan<&str>, (), ParseError> {
            crate::context_expected(
                format!($expected),
                nom::combinator::map(
                    nom::bytes::complete::tag($tag),
                    |_| ()
                )
            )(input)
        }
    };
}

pub(crate) use make_expected_token_parser;



make_expected_token_parser!(expected_semicolon, "semicolon \";\"", ";");
make_expected_token_parser!(expected_double_colon, "double colon \"::\"", "::");
make_expected_token_parser!(expected_comma, "comma \",\"", ",");
make_expected_token_parser!(expected_left_paren, "left parenthesis \"(\"", "(");
make_expected_token_parser!(expected_right_paren, "right parenthesis \")\"", ")");



#[cfg(test)]
mod tests {
    use crate::expected_semicolon;


    #[test]
    fn symbols() {
        eprintln!("{:?}", expected_semicolon(";".into()));
        eprintln!("{:?}", expected_semicolon("   ;".into()));
        eprintln!("{:?}", expected_semicolon(";;".into()));
        eprintln!("{:?}", expected_semicolon("-".into()));
    }

}



trait Parse {
    fn subparser_name() -> String;

    fn parse(
        input: LocatedSpan<&str>
    ) -> IResult<LocatedSpan<&str>, Self, ParseError> where Self: Sized;

    /// Parse into `Self`, with a helpful error message upon failure.
    /// 
    /// Use this when you are implementing `Parse` for a different type.
    fn subparse(
        input: LocatedSpan<&str>
    ) -> IResult<LocatedSpan<&str>, Self, ParseError> where Self: Sized {
        context_subparser(Self::subparser_name(), Self::parse)(input)
    }
}



macro_rules! impl_from_str {
    ($ty:ty) => {
        impl std::str::FromStr for $ty {
            type Err = crate::ParseError;
        
            fn from_str(s: &str) -> std::result::Result<$ty, Self::Err> {
                let res = crate::finish_with(
                    <$ty as crate::Parse>::parse
                )(s.into());

                match res {
                    Ok((_, res)) => Ok(res),
                    Err(nom::Err::Incomplete(_)) => unreachable!(),
                    Err(nom::Err::Error(e)) => Err(e),
                    Err(nom::Err::Failure(e)) => Err(e),
                }
            }
        }
    };
}

pub(crate) use impl_from_str;



pub mod feature;
pub mod lexical_entry;
pub mod mg;
pub mod deriv;
