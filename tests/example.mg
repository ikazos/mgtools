#   Hashtag starts a comment that spans the rest of that line.



#   The first thing you specify is the starting category, followed by a semicolon.
c;

#   Then you specify the lexical entries.
#   Each lexical entry is a PF, then a double colon, then a feature sequence, finally a semicolon.
john :: d;
mary :: d;
who :: d -wh;

#   Spaces and newlines don't matter.
#   Here's a really ugly way to write "likes :: =d =d v;":
            likes         ::=d                  =d
v


;

#   This is prettier:
sings :: =d v;

#   If a word has an empty PF, simply begin that lexical entry with a double colon.
:: =v c;
:: =v +wh c;